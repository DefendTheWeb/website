$(function() {

    // Scroll message window to bottom
    var $conversation = $('.messages-conversation-content');
    if ($conversation.length) {
        var top = $conversation[0].scrollHeight;
        $conversation.animate({ scrollTop: top }, 200);
    }

    // Scroll list view to current message
    var $target = $('.messages-list-item--active');
    if ($target.length) {
        top = $target.offset().top - 100;
        $('.messages-list').animate({ scrollTop: top }, 200);
    }



    // $('.messages-list-item').on('click', function(e) {
    //     e.preventDefault();

    //     $('.messages-conversation').empty();

    //     var url = $(this).attr('href');

    //     $('.messages-list-item--active').removeClass('messages-list-item--active');
    //     $(this).addClass('messages-list-item--active').removeClass('messages-list-item--new');

    //     $.get(url + '?ajax', function(html) {
    //         $('.messages-conversation').html(html);

    //         setupMarkdownEditors();

    //         var top = $('.messages-conversation-content')[0].scrollHeight;
    //         $('.messages-conversation-content').animate({ scrollTop: top }, 200);
    //     });
    // });
});