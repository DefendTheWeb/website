// Check if user has new notifcations
if ($('meta[name="csrf-token"]').length) {
    var $notifcationIndicator = $('.nav-notifications'),
        $pmIndicator = $('#sidebar a[href="/messages"] .tag');
    setInterval(function() {
        $.get(document.dtw.api + 'notifications/unread', function(data) {
            if (data.data.count) {
                $notifcationIndicator.addClass('nav-notifications--unread');
            } else {
                $notifcationIndicator.removeClass('nav-notifications--unread');
            }

            if (data.data.pm > 0) {
                $pmIndicator.text(data.data.pm).removeClass('tag--secondary-alt').addClass('tag--primary');
            } else {
                $pmIndicator.text(0).addClass('tag--secondary-alt').removeClass('tag--primary');
            }
        });
    }, 15000);
}

// Update class when notification is opened
$(".notification a[href*='?notification=']").mousedown(function(e) {
    if (e.which == 1 || e.which == 2) {
        $(this).closest('.notification').removeClass('notification--unread');
    }
});

var notificationTImer;
$('.nav-notifications .popup-handle').on('click', function(e) {
    if (!$(this).parent().hasClass('nav-notifications--unread')) {
        e.stopImmediatePropagation();
        return;
    }

    if ($(this).hasClass('popup-handle--active')) {
        return;
    }

    // Load notifications
    var $popup = $('.nav-notifications-popup .notifications');

    $popup.empty().removeClass('center');

    clearTimeout(notificationTImer);
    notificationTImer = setTimeout(function() {
        $popup.addClass('popup--loading');
    }, 1000);

    $.get(document.dtw.api + 'notifications/unread?full', function(data) {
        clearTimeout(notificationTImer);
        if (data.data.unread) {
            // Add clear all link
            $('<a/>', { class: 'right', text: 'Mark all read', 'href': '#' }).appendTo($popup).on('click', function(e) {
                e.preventDefault();

                $.get(document.dtw.api + 'notifications/markread');
                $('.nav-notifications .popup-handle').trigger('click'); // Hide popup
                $('.notification').closest('.notification').removeClass('notification--unread');
                $('.nav-notifications').removeClass('nav-notifications--unread');
            });


            $.each(data.data.unread, function(title, messages) {
                if (!messages.length) return;

                $('<h3>', { text: title }).appendTo($popup);

                $.each(messages, function(index, notification) {
                    $('<div>', { class: 'notification', html: notification }).appendTo($popup);
                });
            });
        }

        $popup.removeClass('popup--loading');
    });
});