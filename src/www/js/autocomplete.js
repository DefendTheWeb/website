$(function() {

    $('[data-autocomplete]').on('keyup', function() {
        var $target = $(this);

        var values = $target.val().split(',').map(function(s) { return s.trim() });
 
        autocomplete.search('users', values[values.length-1], {
            top: $target.offset().top + $target.outerHeight(),
            left: $target.offset().left,
            width: $target.outerWidth()
        }, function(username) {
            values.pop()
            values.push(username);
            $target.siblings('.autocomplete-output').remove();
            $target.val(values.join(', ') + ', ').focus();
        });
    });
});

var autocomplete = {};

autocomplete.remove = function() {
    if (this.$output) {
        this.$output.remove();
        this.$output = null;
    }
}

autocomplete.search = function(subject, search, position, callback) {
    var self = this;

    // Generate output
    if (!this.$output) {
        this.$output = $('<div>', { class: 'autocomplete-output' }).hide().appendTo($('body'));
        this.$output.append($('<ul>'));
    }

    if (position) {
        this.$output.css(position);
    }

    $.get(document.dtw.api + subject + '/lookup', {
        q: search
    }).done(function(data) {
        if (!data.data || !data.data.results || !data.data.results.length) {
            self.remove();
            return;
        }

        var $ul = self.$output.show().find('ul').empty().append(
            data.data.results.map(function(result) {
                return $("<li>", { text: result.text, 'data-replace': result.replace }).on('click', function() {
                    callback($(this).data('replace'));

                    self.remove();
                });
            })
        );
    }).fail(function() {
        self.remove();
    });
}

$(document).on('click', function (e) {
    if ($(e.target).closest(".autocomplete-output").length === 0) {
        autocomplete.remove();
    }
});