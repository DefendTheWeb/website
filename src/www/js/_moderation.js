$(function() {
    var selectedTickets = [],
        lastSelectedTicket = null,
        $checkboxes = $('.moderation-tickets input[type=checkbox]');

    $checkboxes.on('click', function(e) {
        if (!$(this).is(':checked')) {
            lastSelectedTicket = null;
        } else {
            if (e.shiftKey && lastSelectedTicket) {
                var start = $checkboxes.index(this);
                var end = $checkboxes.index(lastSelectedTicket);

                $checkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastSelectedTicket.checked);
            }
            lastSelectedTicket = this;
        }

        // Update styling for selected
        $('.moderation-tickets input[type=checkbox]:not(:checked)').closest('.feed-item').removeClass('active');

        var $selected = $('.moderation-tickets input[type=checkbox]:checked').closest('.feed-item');
        $selected.addClass('active');

        selectedTickets = $selected.map(function() {
           return $(this).data('id');
        }).get();

        if (selectedTickets.length) {
            $('body').addClass('show-multiple-actions');
            $('.multiple-actions strong').text('Mark ' + selectedTickets.length + ' as:');
        } else {
            $('body').removeClass('show-multiple-actions');
        }
    });


    $('.multiple-actions button[data-action]').on('click', function() {
        if (!selectedTickets.length) return;

        $.post(document.dtw.api + 'moderation/tickets', {
            action: $(this).data('action'),
            tickets: selectedTickets
        }).done(function() {
            window.location.reload();
        }).fail(function(response) {
            if (JSON.parse(response.responseText)) {
                var data = JSON.parse(response.responseText);
                alert("Error: " + data.error.msg);
            } else {
                alert("Error");
            }
        });
    });
});