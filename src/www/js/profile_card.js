$(function() {

    var profileCardTimeout;
    $('[data-profile-card]').on('mouseenter', function() {
        var $this = $(this),
            username = $this.data('profile-card'),
            position = $this.offset(),
            height = $this.height() + 15;

        profileCardTimeout = setTimeout(function() {

            $.get(document.dtw.api + 'user/card', { id: username }, function(data) {
                $('.user-profile-popup').remove();
                $('<div>', { class: 'user-profile-popup', html: data.data.html })
                    .css({
                        top: position.top + height,
                        left: position.left,
                    })
                    .appendTo($('body'));

                document.dtw.lazy();
            });

        }, 500);
    }).on('mouseleave', function() {
        $('.user-profile-popup').remove();

        clearTimeout(profileCardTimeout);
    });


    // Handle following/unfollowing users
    $('[data-follow]').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);

        $.ajax({
            type: "POST",
            url: $(this).attr('href') + '?ajax',
            dataType: "json",
            success: function(data) {
                if (data.type === 'success') {

                    if ($this.text() === 'Follow') {
                        $this.text('Stop following').addClass('button--warning');
                    } else {
                        $this.text('Follow').removeClass('button--warning');
                    }

                }
            }
        });
    });

});