$(function() {

    $('.dashboard-news-navigation a').on('click', function(e) {
        var $this = $(this),
            action = $this.text();

        console.log(action);

        if (action == 'newer' || action == 'older') {
            e.preventDefault();

            if (!$this.hasClass('active')) return;
        } else {
            return;
        }

        var $current = $('.dashboard-news-page:visible');

        if (action == 'newer') {
            $target = $current.prev('.dashboard-news-page');
        } else {
            $target = $current.next('.dashboard-news-page');
        }

        if (!$target.length) {
            return;
        }

        $current.hide();
        $target.css('display', 'flex');

        $(this).siblings('a').addClass('active');

        $(this).removeClass('active');
        if (action == 'newer' && $target.prev('.dashboard-news-page').length) {
            $(this).addClass('active');
        } else if (action == 'older' && $target.next('.dashboard-news-page').length) {
            $(this).addClass('active');
        }
    });

});