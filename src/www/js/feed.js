$(function() {

    if ($('meta[name="site-websocket"]').length && $('.feed--global').length) {

        var $feed = $('.feed--global .block-content');

        $.get(document.dtw.api + 'feed/history').done(function(response) {
            $feed.empty();

            response.data.reverse().forEach(function(item) {
                appendFeedItem(item, $feed);
            });
        });

        var websocketURL = $('meta[name="site-websocket"]').attr("content"),
            websocket = new Centrifuge('wss://' + websocketURL + '/connection/websocket');

        websocket.subscribe("feed", function(message) {
            appendFeedItem(message.data, $feed, true);
        });

        websocket.connect();

        function appendFeedItem(item, $feed, animate) {
            var date = new Date(item.timestamp),
                since = Math.floor(Math.abs(new Date() - date) / (1000 * 60));

            var message;
            switch(item.type) {
                case 'playground.complete': message = "Completed <a href='" + item.permalink + "'>" + item.level + "</a>"; break;
                case 'user.join': message = "Joined site"; break;
                case 'discussion.thread': message = "Created <a href='" + item.permalink + "'>" + item.thread + "</a>"; break;
                case 'discussion.post': message = "Posted in <a href='" + item.permalink + "'>" + item.thread + "</a>"; break;
            }
            if (!message) return;

            var $feedItem = $('<div>', { class: "feed-item" }),
                $feedItemMain = $('<div>', { class: "feed-item-main" }).appendTo($feedItem),
                $feedItemHeader = $('<div>', { class: "feed-item-header" }).appendTo($feedItemMain),
                $feedItemAvatar = $('<a>', { class: "feed-item-avatar nounderline" }).appendTo($feedItemHeader);
            $feedItemDetails = $('<div>', { class: "feed-item-details" }).appendTo($feedItemHeader),

            $feedItemAvatar.append($('<img />', { src: item.image }));

            $('<div>')
                .append($('<a>', { href: "/profile/" + item.user, text: item.user, target: "_blank" }))
                .append($('<span>', {
                    class: "small right time",
                    "data-timestamp": item.timestamp,
                    text: since ? since + " min" + (since == 1 ? '' : 's') : "secs"
                }))
                .appendTo($feedItemDetails);
            $('<div>', { class: "small", html: message })
                .appendTo($feedItemDetails);

            if (animate) {
                $feedItem.hide().css('opacity', 0).prependTo($feed).slideDown().animate(
                    {opacity: 1},
                    {
                        queue: false,
                        complete: function () {
                            $feed.children(":nth-child(5)").nextAll().remove();
                        }
                    }
                );
            } else {
                $feedItem.prependTo($feed);
                $feed.children(":nth-child(5)").nextAll().remove();
            }
        }


        setInterval(function() {
            var now = new Date();
            $feed.find('.time[data-timestamp]').each(function() {
                var date = new Date($(this).data('timestamp')),
                    since = Math.floor(Math.abs(now - date) / (1000 * 60));

                $(this).text(since ? since + " min" + (since == 1 ? '' : 's') : "secs");
            });

        }, 10000);
    }

});