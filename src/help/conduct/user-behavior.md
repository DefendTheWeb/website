To help keep this site a clean and pleasant place to visit we need your help! We actively try and moderate the site but often things are missed. If you see any behavior that has no place here please report it to a moderator and they will be able to deal with it.

### Discussion
You can flag a discussion post or thread directly to our moderators using the 'report' link on each post. This can be found under the dropdown menu in the top right of the post.

It will ask you to select an appropriate reason you feel the content is not appropriate. If none of these match your reason select 'other' and fill in as much detail as possible to justify your report.

### Private messages
You can block a user via their profile to stop them being allowed to send you new private messages. Also if you are getting a lot of unwanted messages you can switch privacy settings to only allow new PMs from users you are following.

Currently there is no way to report user behavior directly from private messages however you can report it via our [contact us](/help/contact) page.

### User profiles
Currently there is no way to report a user's profile directly however you can report it via our [contact us](/help/contact) page.