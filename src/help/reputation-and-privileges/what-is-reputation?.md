### What is reputation?
Reputation is a rough measurement of how much the community trusts you; it is earned by convincing your peers that you know what you’re talking about. The more reputation you earn, the more privileges you gain and the more tools you'll have access to on the site - at the highest privilege levels, you'll have access to many of the same tools available to the site moderators. That is intentional. We don’t run this site; the community does!

### How is reputation calculated?

Currently reputation is awarded by:

* Completing levels
* Earning medals
* Having an article published
* Contributing in discussions

### Why should I care?
You shouldn't... reputation is primary used to indicate to other users how knowledgeable you are with the site and topics therein. It is not meant to be a competition! Reputation also unlocks privileges but these are mainly for moderator tasks.

### My reputation doesn't seem right!
As the reputation is calculated from a number of different sources it can sometimes get out of sync. If you think that your reputation isn't calculating correctly please let us know via our [contact us](/help/contact)</a> form.