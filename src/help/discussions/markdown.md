## Headers

Headers can be specified by typing characters under text or by adding leading hash characters.

Text can be set as a header by prefix it with hashes. The number of hash characters determines the level of heading.

```
# This is an H1
## This is an H2
### This is an H3
```

## Emphasis

Any words or sections of words surrounded by asterisks or underlines are highlighted for emphasis.

Text with a single '\*' or '\_' preceding and following the text are wrapped with HTML `<em>` tags. A double '\*\*' or '\_\_' will result in the text being wrapped with `<strong>` tags. 

```
*I will be italicized*
_So will I_
**I will be bolded**
__So will I__
```

*I will be italicized*
_So will I_
**I will be bolded**
__So will I__

You can escape the '\*' and '\_' if you want it output instead of interpreted, e.g., '\\\*'.


## Links

Links are written in either "inline" (immediate) style or in "reference" style. The link text is written in square quotes, e.g., "[My link text]". For immediate links, you follow the square brackets with the URL in parentheses.

```
[DuckDuckGo](https://duckduckgo.com/)
```

There is a shorthand for when you just want URLs to be clickable. Surround the URL with angled brackets:

```
<https://duckduckgo.com/>
```

## Images

Images can be uploaded to a markdown area by simply dragging and dropping. Uploaded images will have the correct markdown syntax generated automatically.

External images are included just like links but the opennign square bracket is prefixed with an exclamation point.

```
![Alt text](http://images.com/image.png)
```

Image dimensions can also be provided. After the URL, type a space and then an equals and the width and height separated by a 'x'. As images on Defend the Web are set to have width auto only the height is taken into account:

```
  ![Alt text](http://images.com/image.png =100x150)
```

## Block quotes

Block quotes are specified by A leading greater than sign ('>') and space is placed at the beginning of each block quoted line.

```
> This is a block quoted section of text.  It can wrap,
> to later lines.
>
> Blank lines need to be prefixed too.
```

> This is a block quoted section of text.  It can wrap,
> to later lines.
>
> Blank lines need to be prefixed too.


## Lists

Unordered lists have leading asterisks, pluses or dashes.

```
* Item 1
* Item 2
* Item 3
```

* Item 1
* Item 2
* Item 3

## Code

Code can be specified in a block or inline.

Any text indented by four spaces or fenced by three backtick quotes (\`\`\`) will display as a block of code.

    ```
    a = x(45)
    b = a + 2
    c = a * b
    ```

```
a = x(45)
b = a + 2
c = a * b
```


An "inline" code definition has a single backtick before and after the portion of code:

```
`a = x(45)`
```
`a = x(45)`


## Tables

Simple tables can be written out using dashes and pipes.

```
aaa | bbbb
-----|------
hello|sailor
next | line
```

aaa | bbbb
-----|------
hello|sailor
next | line

## Spotify

Links to songs or playlists on Spotify will automatically be converted to embedded players.

```
https://open.spotify.com/user/cyberzoo78/playlist/4VD06iQuXulKarrsIHia40?si=HwM34nGFTJSEOHdQX7_UDA
```

https://open.spotify.com/user/cyberzoo78/playlist/4VD06iQuXulKarrsIHia40?si=HwM34nGFTJSEOHdQX7_UDA

## Asciinema

Links to asciinema recordings will automatically be converted to embedded players.

```
https://asciinema.org/a/168512
```

https://asciinema.org/a/168512