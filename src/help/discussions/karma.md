Karma is a way to rate users posts within the forum. The karma rating is displayed as a number on the left of each forum post. Users with the bronze Karma medal are allowed to upvote posts and only users with the silver Karma medal can down vote.

You earn the medals by completing levels and being active within the forum.