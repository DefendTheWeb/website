Firstly, why aren't you using a password manager?! We will let you off this once, you can recover your password using our [recovery page](/auth/forgot). Enter your email or username and you will recieve and email with instructions on how to recover your account.

If you are logging via a third party we can't help you. You will have to contact them directly.