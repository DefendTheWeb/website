The official Defend the Web chat rooms are run on [Matrix](https://matrix.org/), an open network for secure, decentralized communication.

## What is Matrix?
Matrix is an open standard for interoperable, decentralised, real-time communication over IP. Matrix defines the standard, and provides open source reference implementations of Matrix-compatible Servers, Client SDKs and Application Services to help you create new communication solutions or extend the capabilities and reach of existing ones.

Federation allows separate deployments of a communication service to communicate with each other - for instance a mail server run by Google federates with a mail server run by Microsoft when you send email from @gmail.com to @hotmail.com.

Matrix provides open federation - meaning that anyone on the internet can join into the Matrix ecosystem by deploying their own server.

## How do I join in?

Our main room can be found at #hq:defendtheweb.co.uk. If you are already a Matix user simply join our channel and start talking. If you have never used Matrix before you will need to login to a homeserver to get started.

Homeservers are where a user stores their communication history and account information. It also shares data with the wider Matrix ecosystem by synchronising communication history with other homeservers. Defend the Web hosts it's own homeserver that is open to any DtW user with their account username and password

An embedded JS client is included [here](/chat). Simply set the custom homeserver to https://defendtheweb.co.uk (note this is different to the main sites URL) and login with your DtW account.


### Using a different client
Matrix just the underlying protocol used by our chat network and does not need to be accessed via the Riot client. A list of clients can be found at https://matrix.org/docs/projects/try-matrix-now/.

Riot is the most polished client with releases for web, desktop, iOS and Android and is the easiest way to get started.

### Homeserver settings
* Homeserver URL: https://defendtheweb.co.uk
* Username: Your DtW username
* Password: Your DtW password

### Still love IRC?
An IRC bridge is running to allow users who feel more at home in IRC to join in the conversation. You can connect to the #hackthis IRC channel using settings found [here](https://macak.co.uk/help.html).

## Statistics 
A breakdown of activity in the main #hq channel can be found on the [chat statics](/chat/statistics) page.