## 2008
![HackThis!! 2008](https://zhr3.co.uk/hackthis-logo-2008.jpg)
This site was born as HackThis!! 10<sup>th</sup> January


## 2009
![HackThis!! 2009](https://zhr3.co.uk/hackthis-site-2009.jpg =1x300)
Site redesign

## 2012
Version 5 of the site released. Introducting articles, forum and medals

## 2013
![CTF 2015 flag](https://zhr3.co.uk/ctf-2013-flag.png =1x200)
The first capture the flag competition to celebrate passing 100,000 registered members

![HackThis!! t-shirt](https://zhr3.co.uk/hackthis-shirt-v1.jpg =1x200)
Hand made HackThis!! t-shirts go on sale.

New site online

## 2014
HackThis!! enabled [WeChall](http://www.wechall.net/) integration

## 2015
![HackThis!! 2009](https://zhr3.co.uk/hackthis-site-2015.png =1x300)
Site redesign

We ran a week long capture the flag event with a slection of prizes for the top particpants. [Backup of leaderboard](/ctf/2015). An excellent write up from [dloser](/profile/dloser) can be read [here](/ctf/2015/writeup.html)

## 2018
HackThis!! turns 10

## 2019
![HackThis!! 2019](https://zhr3.co.uk/hackthis-site-2019.png =1x300)
The final day of HackThis!!

![DtW 2019](https://zhr3.co.uk/dtw-site-2019.png =1x300)
HackThis!! is rebranded to Defend the Web