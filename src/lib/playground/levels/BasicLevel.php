<?php
namespace dtw\playground\levels;

class BasicLevel extends BaseLevel {

    public function __construct($level_id) {
        parent::__construct($level_id);

        $this->data->template = 'basic';
    }
}