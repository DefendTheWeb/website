<?php
    namespace dtw\articles;

    class Article extends \dtw\utils\DataObject {
        protected $type = 'article';
        protected $table = 'articles';

        protected function getByslug($slug) {
            // Lookup slug in Redis
            $articleID = \dtw\DtW::$redis->get($this->type . ':slug:' . $slug);

            // If the link wasn't found, check DB
            if (!$articleID) {
                $stmt = \dtw\DtW::$db->prepare('SELECT article_id FROM articles WHERE `slug` = :slug'); 
                $stmt->execute(array(':slug' => $slug)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();
                    $articleID = $row->article_id;
                } else {
                    throw new \Exception('Article not found');
                }
            }

            try {
                $this->getByID($articleID);
            } catch (\Exception $e) {
                throw $e;
            }
        }

        protected function getByID($id, $force=false) {
            // Check if article exists in Redis
            $article = \dtw\DtW::$redis->get($this->type . ':' . $id);

            if ($article && !$force) {
                $this->data = json_decode($article);
                \dtw\DtW::$log->info('article.view', array('title' => $this->data->title, 'source' => 'redis'));
            } else {
                // Rebuild article from DB
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `article_id` AS `id`, `slug`, `title`, `author`, `created`, `submitted`, `published`, `updated`, `content`, `topic_id`, `status`, `image`, `views`
                    FROM articles
                    WHERE `article_id` = :articleID
                '); 
                $stmt->execute(array(':articleID' => $id)); 
                if (!$stmt->rowCount()) {
                    throw new \Exception('Article not found');
                }
                $this->data = $stmt->fetch();
                $this->generate();

                $this->updateCache();
                \dtw\DtW::$log->info('article.view', array('title' => $this->data->title, 'source' => 'DB'));
            }

            $DtW = \dtw\DtW::getInstance();

            // Check user has access
            if (!$this->isViewable()) {
                throw new \Exception('Article not found');
            }

            $this->id = $this->data->id;
        }

        private function generate() {
            $this->data->permalink = \dtw\DtW::$config->get('site.domain') . '/article/' . $this->data->slug;
            $this->data->titleSafe = htmlspecialchars($this->data->title);

            $content = $this->data->content;
            $this->data->content = new \stdClass();
            $this->data->content->plain = $content;
            // $this->data->content->safe = htmlspecialchars($content);
            // $this->data->content->html = $content;

            $this->data->content->words = str_word_count($content);
            $this->data->content->duration = ceil($this->data->content->words / 3 / 60); // 3 words per second average

            // Render content
            $html = \dtw\utils\Markdown::parse($content, true);

            // Replace challenges
            $html = preg_replace_callback("/\[challenge (.+)\]/", function($matches) {
                $path = DIR . 'articles/' . $matches[1];
                return "<div class='article-challenge'>" . file_get_contents($path) . "</div>";
            }, $html);

            $this->data->content->html = $html;
            $this->data->content->safe = htmlspecialchars(strip_tags($html));

            // Check topic
            if ($this->data->topic_id) {
                try {
                    \dtw\Articles::getTopic($this->data->topic_id);
                } catch (\Exception $e) {
                    $this->data->topic_id = 0;
                }
            }
        }

        public function create($data) {
            // trim all data
            $data = array_map('trim', $data);

            if (!isset($data['title']) || empty($data['title']) || !isset($data['content']) || empty($data['content'])) {
                throw new \Exception('All fields are required');
            }

            $data['slug'] = $this->generateSlug($data['title']);

            // Check topic exists
            try {
                \dtw\Articles::getTopic($data['topic']);
            } catch (\Exception $e) {
                $data['topic'] = 0;
            }

            $stmt = \dtw\DtW::$db->prepare('
                INSERT INTO articles (`title`, `slug`, `content`, `author`, `topic_id`)
                VALUES (:title, :slug, :content, :author, :topicID);
            ');
            $stmt->execute(array(
                ':title' => $data['title'],
                ':slug' => $data['slug'],
                ':content' => $data['content'],
                ':author' => \dtw\DtW::getInstance()->user->id,
                ':topicID' => $data['topic']
            ));

            $this->id = \dtw\DtW::$db->lastInsertId();
            $this->getByID($this->id);
            $this->addRevision();
        }

        public function update($update) {
            $DtW = \dtw\DtW::getInstance();

            if (!$this->isEditable()) {
                throw new \Exception('You don\'t have privileges to edit this article');
            }

            // Check topic exists
            try {
                \dtw\Articles::getTopic($data['topic']);
            } catch (\Exception $e) {
                $update['topic'] = 0;
            }

            // Upload image
            if (file_exists($update['image']['tmp_name']) && is_uploaded_file($update['image']['tmp_name'])) {
                $DtW->load('Images');
                try {
                    $update['image'] = $DtW->images->upload($update['image']);
                } catch (Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            } else if (isset($update['removeImage'])) {
                $update['image'] = null;
            } else {
                $update['image'] = $this->data->image;
            }

            // Generate new slug
            $previousslug = $this->data->slug;
            $update['slug'] = $this->generateSlug($update['title'], $previousslug);

            $stmt = \dtw\DtW::$db->prepare('
                UPDATE articles
                SET `title` = :title,
                    `slug` = :slug,
                    `content` = :content,
                    `image` = :image,
                    `topic_id` = :topicID
                WHERE `article_id` = :articleID
            ');
            $stmt->execute(array(
                ':title' => $update['title'],
                ':slug' => $update['slug'],
                ':content' => $update['content'],
                ':image' => $update['image'],
                ':topicID' => $update['topic'],
                ':articleID' => intval($this->id)
            ));

            if (!$this->isPublished()) {
                $this->setStatus('draft', false);
            }

            // Clear current topic cache
            if ($update['topic'] !== $this->data->topic_id) {
                \dtw\DtW::$redis->del('topic:' . $this->data->topic_id);
            }

            // Update article
            $this->getByID($this->id, true);
            $this->clearCache();
            $this->addRevision();

            $this->buildSearch();

            return $update['slug'];
        }

        public function countView() {
            parent::countView();

            $DtW = \dtw\DtW::getInstance();
            if ($DtW->user->isAuth()) {
                \dtw\user\Meta::set('view:article', true, false);
                \dtw\user\Meta::set('view:article:' . $this->data->slug, true, false);
            }
        }

        public function storeViews() {
            $key = $this->type . ':' . $this->data->id . ':views';
            $this->data->views += \dtw\DtW::$redis->sCard($key);

            $stmt = \dtw\DtW::$db->prepare("
                UPDATE {$this->table} SET `views` = :views WHERE `article_id` = :articleID;
            ");
            $stmt->execute(array(
                ':articleID' => $this->data->id,
                ':views' => $this->data->views
            ));

            // Clear set
            \dtw\DtW::$redis->del($key);

            // Update cache
            $this->updateCache();
        }

        public function handleModeration($action, $message = null) {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->hasPrivilege('articles.publish')) {
                throw new \Exception('You don\'t have privileges to moderate this article');
            }

            if ($action == 'publish') {
                // Set published date
                $stmt = \dtw\DtW::$db->prepare('
                    UPDATE articles
                    SET `published` = NOW()
                    WHERE `article_id` = :articleID
                    AND `published` IS NULL
                ');
                $stmt->execute(array(
                    ':articleID' => intval($this->id)
                ));

                // Update article status
                $this->setStatus('published');

                // Notify author
                $notificationData = (object) array(
                    'article' => $this->id,
                    'status' => 'published'
                );

                \dtw\Notifications::send($this->data->author, 'article.moderation', $notificationData);

                // Add to feed
                \dtw\utils\Feed::addItemToFeed(array(
                    'type' => 'article.published',
                    'user' => $this->data->author,
                    'article' => intval($this->id)
                ));

                // Check users medals
                \dtw\Medals::checkUsersMedals('articles', $this->data->author);
            } else if ($action == 'decline') {
                if (!$message) {
                    throw new \Exception('Reason is required to decline article');
                }

                // Update article status
                $this->setStatus('declined');

                $this->setRevisionNote($message);

                // Notify author
                $notificationData = (object) array(
                    'article' => $this->id,
                    'status' => 'declined'
                );

                \dtw\Notifications::send($this->data->author, 'article.moderation', $notificationData);
            }
        }

        public function setStatus($status, $updateRevision = true) {
            if (!$this->isEditable()) {
                throw new \Exception('Article not found');
            }

            $stmt = \dtw\DtW::$db->prepare('
                UPDATE articles
                SET `status` = :status
                WHERE `article_id` = :articleID
            ');
            $stmt->execute(array(
                ':status' => $status,
                ':articleID' => intval($this->id)
            ));
            $this->data->status = $status;

            if ($status == 'review') {
                // Set submitted date
                $stmt = \dtw\DtW::$db->prepare('
                    UPDATE articles
                    SET `submitted` = NOW()
                    WHERE `article_id` = :articleID
                    AND `submitted` IS NULL
                ');
                $stmt->execute(array(
                    ':articleID' => intval($this->id)
                ));
            }

            if ($updateRevision) {
                // Update revision status
                $stmt = \dtw\DtW::$db->prepare('
                    UPDATE article_revisions
                    SET `status` = :status
                    WHERE `article_id` = :articleID
                    ORDER BY `revision_id` DESC
                    LIMIT 1
                ');
                $stmt->execute(array(
                    ':status' => $status,
                    ':articleID' => intval($this->id)
                ));
            }

            // Add to search
            $this->buildSearch();

            $this->clearCache();

            return true;
        }

        protected function updateCache($previousSlug = NULL) {
            if ($this->revision) {
                return;
            }

            parent::updateCache($previousSlug);
        }

        private function clearCache() {
            // Clear redis cache
            \dtw\DtW::$redis->del('articles');
            \dtw\DtW::$redis->del($this->type . ':' . $this->data->id);
            \dtw\DtW::$redis->del('topic:' . $this->data->topic_id);

            // Clear top picks
            \dtw\DtW::$redis->del('articles:picks');

            // Delete static cache
            $url = substr($_SERVER['REQUEST_URI'], 0, -5);
            \dtw\utils\StaticCache::delete($url);

            \dtw\utils\StaticCache::delete('/articles');
            \dtw\utils\StaticCache::delete('/dashboard');
        }

        private function addRevision() {
            $DtW = \dtw\DtW::getInstance();

            // Store revision
            $stmt = \dtw\DtW::$db->prepare('
                INSERT INTO article_revisions (`article_id`, `title`, `content`, `status`, `user_id`)
                VALUES (:articleID, :title, :content, :status, :userID);
            ');
            $stmt->execute(array(
                ':articleID' => $this->id,
                ':title' => $this->data->title,
                ':content' => $this->data->content->plain,
                ':status' => $this->data->status,
                ':userID' => $DtW->user->id
            ));
        }

        private function setRevisionNote($message) {
            // Set submitted date
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE article_revisions
                SET `log` = :message
                WHERE `article_id` = :articleID
                ORDER BY `revision_id` DESC
                LIMIT 1
            ');
            $stmt->execute(array(
                ':articleID' => intval($this->id),
                ':message' => $message
            ));
        }

        public function getTopic() {
            if (!$this->data->topic_id) {
                return (object)array(
                    'title' => 'Your articles',
                    'permalink' => '/articles/drafts'
                );
            }

            return \dtw\Articles::getTopic($this->data->topic_id);
        }

        public function getRevisions() {
            if (!$this->isEditable()) {
                throw new \Exception("You do not have access to this articles revisions");
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT *
                FROM article_revisions
                WHERE `article_id` = :articleID
                ORDER BY `date` DESC
            '); 
            $stmt->execute(array(':articleID' => $this->id)); 
            if ($stmt->rowCount()) {
                $revisions = $stmt->fetchAll();
                
                return $revisions;
            }
        }

        public function getRevision($revision) {
            if (!$this->isEditable()) {
                throw new \Exception("You do not have access to this articles revisions");
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT *
                FROM article_revisions
                WHERE `revision_id` = :revisionID
                AND `article_id` = :articleID
            ');
            $stmt->execute(array(':revisionID' => $revision, ':articleID' => $this->id)); 
            if ($stmt->rowCount()) {
                $row = $stmt->fetch();

                $this->data = (object) array_merge((array) $this->data, (array) $row);

                $this->revisionID = $revision;
                $this->generate();

            } else {
                throw new \Exception('Revision not found');
            }

        }

        public function isAuthor() {
            $DtW = \dtw\DtW::getInstance();
            return ($this->data->author == $DtW->user->id);
        }

        public function isViewable() {
            return ($this->isPublished() || $this->isEditable() || $this->isAuthor());
        }

        public function isEditable() {
            $DtW = \dtw\DtW::getInstance();

            if (($this->isAuthor() && $this->isDraft()) || $DtW->user->hasPrivilege('articles.edit')) {
                return true;
            }

            return false;
        }

        public function isDeclined() {
            return $this->data->status == 'declined';
        }

        public function isDraft() {
            return ($this->data->status == 'draft' || $this->data->status == 'declined');
        }

        public function isPublished() {
            return ($this->data->status == 'published');
        }

        public function isInReview() {
            return ($this->data->status == 'review');
        }

        public function buildSearch() {
            \dtw\DtW::getInstance()->load('Search');

            $params = [
                'index' => 'articles',
                'id' => $this->id
            ];
            if ($this->isPublished()) {
                $params['body'] = ['title' => $this->title, 'content' => $this->data->content->plain, 'published' => strtotime($this->published)];
                try {
                    \dtw\DtW::getInstance()->search->client->index($params);
                } catch (\Exception $e) {
                    //
                }
            } else {
                try {
                    \dtw\DtW::getInstance()->search->client->delete($params);
                } catch (\Exception $e) {
                    //
                }
            }
        }
    }