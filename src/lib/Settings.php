<?php
    namespace dtw;

    class Settings {

        public static function getIndex() {
            $root = realpath(dirname($_SERVER['DOCUMENT_ROOT'], 1) . '/help');
            $index = array();

            $index['Account'] = [
                [
                    'title' => 'Update email',
                    'permalink' => '/settings/email'
                ],
                [
                    'title' => 'Change password',
                    'permalink' => '/settings/password'
                ],
                [
                    'title' => 'Social sign-in',
                    'permalink' => '/settings/oauth'
                ],
                [
                    'title' => 'Manage 2FA',
                    'permalink' => '/settings/2fa'
                ]
            ];

            $index['Profile'] = [
                [
                    'title' => 'Edit profile',
                    'permalink' => '/settings/profile'
                ],
                [
                    'title' => 'Privacy settings',
                    'permalink' => '/settings/privacy'
                ],
                [
                    'title' => 'Manage followed users',
                    'permalink' => '/settings/followed'
                ],
                [
                    'title' => 'View followers',
                    'permalink' => '/settings/followers'
                ],
                [
                    'title' => 'Change username',
                    'permalink' => '/settings/username'
                ],
                [
                    'title' => 'Userbars',
                    'permalink' => '/settings/userbars'
                ]
            ];

            $index['Notifications'] = [
                [
                    'title' => 'Manage notifications',
                    'permalink' => '/settings/notifications'
                ],
                [
                    'title' => 'Manage emails',
                    'permalink' => '/settings/emails'
                ]
            ];

            $index['Developers'] = [
                [
                    'title' => 'API keys',
                    'permalink' => '/api'
                ],
                [
                    'title' => 'API documentation',
                    'permalink' => '/help/developers/api-documentation'
                ]
            ];

            $index['Housekeeping'] = [
                [
                    'title' => 'Request your data',
                    'permalink' => '/settings/data'
                ],
                [
                    'title' => 'Delete account',
                    'permalink' => '/settings/delete'
                ]
            ];

            return $index;
        }


        public static function getPage($page) {
            switch ($page) {
                case 'email': return self::getPageEmail();
                case 'password': return self::getPagePassword();
                case '2fa': return self::getPage2FA();
                case 'profile': return self::getPageProfile();
                case 'privacy': return self::getPagePrivacy();
                case 'username': return self::getPageUsername();
                case 'delete': return self::getPageDelete();
                case 'notifications': return self::getPageNotifications();
                case 'emails': return self::getPageEmails();
            }
        }


        private static function getPageEmail() {
            $user = \dtw\DtW::getInstance()->user;

            $breadcrumb = array(
                'Settings' => '/settings',
                'Update email' => '/settings/email'
            );

            $form = new \dtw\utils\Form("Update email", "Set email");
            $form->addField('Current password', "password");
            $form->addField('Email', "email", array(
                "value" => $user->email
            ));
            $form->addField('Re-enter email', "email");

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }


        private static function getPagePassword() {
            $breadcrumb = array(
                'Settings' => '/settings',
                'Change password' => '/settings/password'
            );

            $form = new \dtw\utils\Form("Change password", "Set password");
            if (!isset($_SESSION['allowPasswordReset']) && !$_SESSION['allowPasswordReset']) {
                $form->addField('Current password', "password");
            }
            $form->addField('New password', "password");
            $form->addField('Re-enter new password', "password");

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }


        private static function getPage2FA() {
            $user = \dtw\DtW::getInstance()->user;

            $breadcrumb = array(
                'Settings' => '/settings',
                'Manage 2FA' => '/settings/2fa'
            );

            if (!$user->twoFactor) {
                $twoFactor = new \dtw\utils\TwoFactor();
                if (isset($_SESSION['settings.2fa'])) {
                    $secret = $_SESSION['settings.2fa'];
                } else {
                    $secret = $twoFactor->createSecret();
                    $_SESSION['settings.2fa'] = $secret;
                }

                $form = new \dtw\utils\Form("Mange 2FA", "Enable 2FA");

                $url = $twoFactor->getURL("Defend the Web", $user->username, $secret);
                $extras = (object) [
                    "src" => $twoFactor->getQR("Defend the Web", $user->username, $secret),
                    "description" => "<p>Scan the QR code below with the two-factor authentication app on you phone. Alternatively click the code below or enter it manually.</p><pre><a href='" . $url . "'>" . $secret . "</a></pre>"
                ];
                $form->addField("", "image", $extras);

                $extras = (object) [
                    "name" => "code",
                    "description" => "Enter the code that appears in the app here to confirm everything is setup correctly."
                ];
                $form->addField('Enter the code from the app', "text", $extras);
            } else {
                $form = new \dtw\utils\Form("Mange 2FA", "Disable 2FA");
                $extras = (object) [
                    "name" => "code",
                    "description" => "<div class='msg msg--success'>Two factor authentication is enabled.</div>Enter the code from your authenticator app to disable 2FA on this account."
                ];
                $form->addField("Enter code to disable", "text", $extras);
            }

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }


        private static function getPageProfile() {
            $user = \dtw\DtW::getInstance()->user;

            $breadcrumb = array(
                'Settings' => '/settings',
                'Profile' => '/profile',
                'Edit' => '/settings/profile'
            );

            // Profile form
            $form = new \dtw\utils\Form("Profile");
            $form->submit = 'Update';
            $form->description = 'All of the fields on this page are optional and can be deleted at any time, and by filling them out, you\'re giving us consent to share this data wherever your user profile appears. Please see our privacy statement to learn more about how we use this information.';

            $extras = (object) [
               "value" => $user->profile->name,
               "length" => 64
            ];
            $form->addField('Name', "text", $extras);

            $extras = (object) [
               "value" => $user->profile->website
            ];
            $form->addField('Website', "text", $extras);

            $extras = (object) [
               "src" => $user->profile->avatar,
               "size" => "bigavatar",
               "name" => "avatar"
            ];
            $form->addField('Profile image', "upload", $extras);

            $extras = (object) [
               "value" => $user->profile->status
            ];
            $form->addField('Status', "text", $extras);

            $extras = (object) [
               "value" => $user->profile->bio
            ];
            $form->addField('Bio', "markdown", $extras);

            $extras = (object) [
               "value" => $user->profile->signature
            ];
            $form->addField('Signature', "markdown", $extras);

            $form->addField('Activity feed on profile', "toggle", array(
                'name' => 'feedVisible',
                'value' => $user->profile->feedVisible,
                'options' => array(
                    1 => 'Show',
                    0 => 'Hide'
                )
            ));

            $form->addField('Follower counts', "toggle", array(
                'name' => 'followersVisible',
                'value' => $user->profile->followersVisible,
                'description' => 'This will stop users being able to follow you. Your name will still appear on user profiles that you follow.',
                'options' => array(
                    1 => 'Show',
                    0 => 'Hide'
                )
            ));

            $form->addField('Medals on profile', "toggle", array(
                'name' => 'medalsVisible',
                'value' => $user->profile->medalsVisible,
                'options' => array(
                    1 => 'Show',
                    0 => 'Hide'
                )
            ));

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }


        private static function getPagePrivacy() {
            $user = \dtw\DtW::getInstance()->user;

            $breadcrumb = array(
                'Settings' => '/settings',
                'Privacy' => '/settings/privacy'
            );

            // Profile form
            $form = new \dtw\utils\Form("Privacy");
            $form->submit = 'Update';

            $form->addField('Appear in leaderboards', "toggle", array(
                'name' => 'privacy.leaderboard',
                'value' => $user->settings->get('privacy.leaderboard'),
                'options' => array(
                    0 => 'Show',
                    1 => 'Hide'
                )
            ));

            $form->addField('Online status', "toggle", array(
                'name' => 'privacy.online',
                'value' => $user->settings->get('privacy.online'),
                'options' => array(
                    0 => 'Show',
                    1 => 'Hide'
                )
            ));

            $form->addField('PMs from strangers', "toggle", array(
                'name' => 'privacy.pm',
                'value' => $user->settings->get('privacy.pm'),
                'options' => array(
                    0 => 'Allow',
                    1 => 'Block'
                )
            ));

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }


        private static function getPageUsername() {
            $user = \dtw\DtW::getInstance()->user;

            $breadcrumb = array(
                'Settings' => '/settings',
                'Change username' => '/settings/username'
            );

            $form = new \dtw\utils\Form("Change username", "Update username", array(
                'description'=> 'Changing your username can have unintended side effects and has some limitations. <a href="/help/account/username">Learn more</a>'
            ));
            $form->addField('New username', "text",  array(
                "value" => $user->username
            ));

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }


        private static function getPageDelete() {
            $user = \dtw\DtW::getInstance()->user;

            $breadcrumb = array(
                'Settings' => '/settings',
                'Delete account' => '/settings/delete'
            );

            $form = new \dtw\utils\Form("Delete account", "Delete account");
            $form->addField('Password', "password");
            $form->addField('Are you sure?', "toggle", array(
                'name' => 'confirmation',
                'options' => array(
                    'yes' => 'Yes'
                )
            ));

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }

        private static function getPageNotifications() {
            $user = \dtw\DtW::getInstance()->user;

            $breadcrumb = array(
                'Settings' => '/settings',
                'Manage notifications' => '/settings/notifications'
            );

            $form = new \dtw\utils\Form("Notifications", 'Save', array(
                "description" => 'Manage which events will trigger a notification. Some notification types can not be managed or hidden.'
            ));
            $form->addField('Private messages', "toggle", array(
                'name' => 'notifications.pm',
                'value' => $user->settings->get('notifications.pm')
            ));
            $form->addField('New followers', "toggle", array(
                'name' => 'notifications.profile.follow',
                'value' => $user->settings->get('notifications.profile.follow')
            ));
            $form->addField('Discussion reply', "toggle", array(
                'name' => 'notifications.discussion.reply',
                'value' => $user->settings->get('notifications.discussion.reply')
            ));
            $form->addField('Discussion mention', "toggle", array(
                'name' => 'notifications.discussion.mention',
                'value' => $user->settings->get('notifications.discussion.mention')
            ));

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }

        private static function getPageEmails() {
            $user = \dtw\DtW::getInstance()->user;

            $breadcrumb = array(
                'Settings' => '/settings',
                'Manage emails' => '/settings/emails'
            );

            $form = new \dtw\utils\Form("Emails");
            $extras = array(
                'name' => 'emails.notifications',
                'value' => $user->settings->get('emails.notifications')
            );
            $form->addField('Notifications', "toggle", $extras);
            $extras = array(
                'name' => 'emails.news',
                'value' => $user->settings->get('emails.news'),
                "description" => "New products and feature updates, as well as occasional site announcements"
            );
            $form->addField('Features & Announcements', "toggle", $extras);

            return [
                'breadcrumb' => $breadcrumb,
                'form' => $form
            ];
        }

    } 