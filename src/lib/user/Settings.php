<?php
    namespace dtw\user;

    class Settings {
        private $_settings;

        public function __construct($id) {
            $this->id = $id;

            if (isset($_SESSION['userSettings'])) {
                $this->_settings = $_SESSION['userSettings'];
            } else {
                $this->_settings = array();

                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `key`, `value`
                    FROM user_settings
                    WHERE `user_id` = :id
                ');
                $stmt->execute(array(':id' => $id));

                $data = $stmt->fetchAll();

                foreach($data AS $row) {
                    $this->_settings[$row->key] = $row->value;
                }

                $_SESSION['userSettings'] = $this->_settings;
            }

            $defaults = array(
                'notifications.pm' => 1,
                'notifications.profile.follow' => 1,
                'notifications.discussion.mention' => 1,
                'notifications.discussion.reply' => 1,
                'emails.notifications' => 1,
                'emails.news' => 1,
                'privacy.leaderboard' => 0,
                'privacy.online' => 0,
                'privacy.pm' => 0
            );


            $this->_settings = array_merge($defaults, $this->_settings);
        }

        public function save($settings) {
            $allowed = array(
                'notifications.pm',
                'notifications.profile.follow',
                'notifications.discussion.mention',
                'notifications.discussion.reply',
                'emails.notifications',
                'emails.news',
                'privacy.leaderboard',
                'privacy.online',
                'privacy.pm'
            );

            $DtW = \dtw\DtW::getInstance();

            try {
                \dtw\DtW::$db->beginTransaction();

                foreach($settings AS $key => $value) {
                    $key = str_replace("_", ".", $key);

                    if (!in_array($key, $allowed)) {
                        continue; // User is not allowed to update this field
                    }

                    // Update database
                    $stmt = \dtw\DtW::$db->prepare('INSERT INTO `user_settings` (`user_id`, `key`, `value`) VALUES (:id, :key, :value) ON DUPLICATE KEY UPDATE `value` = :value'); 
                    $stmt->execute(array(
                        ':id' => $this->id,
                        ':key' => $key,
                        ':value' => $value
                    ));
                }

                \dtw\DtW::$db->commit();

                unset($_SESSION['userSettings']);
            } catch (Exception $e) {
                \dtw\DtW::$db->rollback();
                throw $e;
            }
        }

        public function get($key) {
            if (array_key_exists($key, $this->_settings)) {
                return $this->_settings[$key];
            }

            return null;
        }

        public function set($key, $value) {
            $stmt = \dtw\DtW::$db->prepare('INSERT INTO `user_settings` (`user_id`, `key`, `value`) VALUES (:id, :key, :value) ON DUPLICATE KEY UPDATE `value` = :value'); 
            $stmt->execute(array(
                ':id' => $this->id,
                ':key' => $key,
                ':value' => $value
            ));

            unset($_SESSION['userSettings']);
        }

        public static function getUserSetting($userID, $name) {
            $defaults = array(
                'notifications.pm' => true,
                'notifications.profile.follow' => true,
                'notifications.discussion.mention' => true,
                'notifications.discussion.reply' => true,
                'emails.notifications' => true,
                'emails.news' => true,
                'privacy.leaderboard' => false,
                'privacy.online' => false,
                'privacy.pm' => false
            );

            $stmt = \dtw\DtW::$db->prepare('
                SELECT value
                FROM user_settings
                WHERE `user_id` = :userID AND `key` = :key
            '); 
            $stmt->execute(array(
                ':userID' => $userID,
                ':key' => $name
            ));

            if ($stmt->rowCount()) {
                $value = $stmt->fetchColumn();
            }

            if (isset($value)) {
                if ($value === '0' || $value === 0) {
                    return false;
                } else if ($value === '1' || $value === 1) {
                    return true;
                } else {
                    return $value;
                }
            } else if (array_key_exists($name, $defaults)) {
                return $defaults[$name];
            } else {
                return null;
            }
        }
    }