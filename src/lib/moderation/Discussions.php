<?php
    namespace dtw\moderation;

    class Discussions {

        public function __construct() {
            if (!\dtw\DtW::getInstance()->user->hasPrivilege('discussions.flags')) {
                throw new \Exception('Invalid permissions');
            }

            \dtw\DtW::getInstance()->load('Discussions');

            $this->queues = array();
            $this->queues['newuser'] = array(
                'title' => 'New users',
                'description' => 'Review posts made by new users',
                'privilege' => 'bronze'
            );
            $this->queues['late'] = array(
                'title' => 'Late posts',
                'description' => 'Review posts in stale discussions',
                'privilege' => 'bronze'
            );
            $this->queues['spam'] = array(
                'title' => 'Spam posts',
                'description' => 'Review posts flagged as spam',
                'privilege' => 'bronze'
            );
            $this->queues['quality'] = array(
                'title' => 'Low quality posts',
                'description' => 'Review posts flagged as being low quality',
                'privilege' => 'silver'
            );
            $this->queues['topic'] = array(
                'title' => 'Off-topic',
                'description' => 'Review posts flagged as being off-topic',
                'privilege' => 'silver'
            );
            $this->queues['other'] = array(
                'title' => 'Other',
                'description' => 'Review posts marked for admin attention',
                'privilege' => 'gold'
            );
        }

        public function getQueues() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*) AS `flags`, `type`
                FROM `forum_flags`
                WHERE `resolved` = 0
                AND `flag_id` NOT IN (SELECT `flag_id` FROM `forum_flags_moderation` WHERE `user_id` = :user_id)
                AND `post_id` NOT IN (SELECT `post_id` FROM `forum_thread_posts` WHERE `deleted` = 1)
                GROUP BY `type`'); 
            $stmt->execute(array(
                ':user_id' => \dtw\DtW::getInstance()->user->id
            ));
            $counts = $stmt->fetchAll();

            foreach($counts AS $count) {
                if (array_key_exists($count->type, $this->queues)) {
                    $this->queues[$count->type]['count'] = $count->flags;
                }
            }

            foreach($this->queues AS &$queue) {
                $queue['access'] = \dtw\DtW::getInstance()->user->hasPrivilege($queue['privilege']);
            }

            return $this->queues;
        }

        public function getQueueFlag($type) {
            if (!array_key_exists($type, $this->queues)) {
                throw new \Exception('Invalid queue');
            } else {
                $queue = $this->queues[$type];
            }

            if (!\dtw\DtW::getInstance()->user->hasPrivilege($queue['privilege'])) {
                throw new \Exception('No access to queue');
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT `forum_flags`.`flag_id`
                FROM `forum_flags`
                WHERE `resolved` = 0
                AND `type` = :type
                AND `flag_id` NOT IN (SELECT `flag_id` FROM `forum_flags_moderation` WHERE `user_id` = :user_id)
                ORDER BY RAND()
                LIMIT 1
            '); 
            $stmt->execute(array(
                ':user_id' => \dtw\DtW::getInstance()->user->id,
                ':type' => $type
            ));
            $item = $stmt->fetch();

            if (!$item) {
                throw new \Exception('Queue is empty');
            }

            return $item->flag_id;
        }

        public function getFlag($type, $flag) {
            if (!array_key_exists($type, $this->queues)) {
                throw new \Exception('Invalid queue');
            } else {
                $queue = $this->queues[$type];
            }

            if (!\dtw\DtW::getInstance()->user->hasPrivilege($queue['privilege'])) {
                throw new \Exception('No access to queue');
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT `forum_flags`.`flag_id`, `forum_flags`.`post_id`, `forum_flags`.`type`, `forum_flags`.`comment`, `forum_flags`.`submitted`, `forum_flags`.`resolved`, `forum_thread_posts`.`thread_id`, `forum_flags_moderation`.`response`
                FROM `forum_flags`
                INNER JOIN `forum_thread_posts`
                ON `forum_thread_posts`.`post_id` = `forum_flags`.`post_id`
                LEFT JOIN `forum_flags_moderation`
                ON `forum_flags`.`flag_id` = `forum_flags_moderation`.`flag_id` AND `forum_flags_moderation`.`user_id` = :user_id
                WHERE `forum_flags`.`flag_id` = :flag
                AND `type` = :type
            ');
            $stmt->execute(array(
                ':flag' => $flag,
                ':type' => $type,
                ':user_id' => \dtw\DtW::getInstance()->user->id
            ));
            $item = $stmt->fetch();

            if (!$item) {
                // Post has already been removed outside of the moderation processes, mark as resolved
                $this->resolveFlag($flag);

                throw new \Exception('Flagged post not found');
            }

            try {
                $thread = \dtw\DtW::getInstance()->discussions->getThread($item->thread_id);
            } catch (\Exception $e) {
                // Post has already been removed outside of the moderation processes, mark as resolved
                $this->resolveFlag($flag);

                throw new \Exception('Thread for flagged post not found');
            }

            if ($item->resolved) {
                $stmt = \dtw\DtW::$db->prepare("
                    SELECT *
                    FROM `forum_flags_moderation`
                    WHERE `flag_id` = :flag;
                ");
                $stmt->execute(array(
                    ':flag' => $flag
                ));
                $item->votes = $stmt->fetchAll();
            } else {
                // Check if flag should be resolved as the post has been removed
                $post = \dtw\DtW::getInstance()->discussions->getPost($thread->id, $item->post_id);
                if ($post->deleted) {
                    $this->resolveFlag($flag);

                    throw new \Exception('Flagged post already removed');
                }
            }

            $queue['flag'] = $item;

            // Load thread info
            try {
                $queue['thread'] = $thread;
            } catch (\Exception $e) {
                // Resolve flag
                $this->resolveFlag($flag);

                throw $e;
            }

            // Load thread posts
            $queue['thread']->getPosts();

            $queue['target'] = array_search($item->post_id, array_column($queue['thread']->posts, 'id'));

            return $queue;
        }

        private function getFlagVotes($flag) {
            $stmt = \dtw\DtW::$db->prepare("
                SELECT
                    SUM(if(response = 'delete', weight, 0)) AS `delete`,
                    SUM(if(response = 'dismiss', weight, 0)) AS `dismiss`,
                    SUM(if(response = 'escalate', weight, 0)) AS `escalate`,
                    SUM(if(response = 'delete', 1, 0)) AS `deleteVotes`,
                    SUM(if(response = 'dismiss', 1, 0)) AS `dismissVotes`,
                    SUM(if(response = 'escalate', 1, 0)) AS `escalateVotes`
                FROM `forum_flags_moderation`
                WHERE `flag_id` = :flag;
            ");
            $stmt->execute(array(
                ':flag' => $flag
            ));
            return $stmt->fetch();
        }

        public function handleFlagAction($type, $flag, $response) {
            if (!array_key_exists($type, $this->queues)) {
                throw new \Exception('Invalid queue');
            } else {
                $queue = $this->queues[$type];
            }

            if (!\dtw\DtW::getInstance()->user->hasPrivilege($queue['privilege'])) {
                throw new \Exception('No access to queue');
            }

            if ($queue == 'other') {
                throw new \Exception('Queue doesnt support actions');
            }

            switch (\dtw\DtW::getInstance()->user->privileges) {
                case 'green': $weight = 15; break;
                case 'gold': $weight = 5; break;
                case 'silver': $weight = 2; break;
                default: $weight = 1; break;
            }

            $stmt = \dtw\DtW::$db->prepare("
                INSERT IGNORE INTO `forum_flags_moderation` (`flag_id`, `user_id`, `response`, `weight`)
                VALUES (:flag_id, :user_id, :response, :weight);
            ");
            $stmt->execute(array(
                ':flag_id' => $flag,
                ':user_id' => \dtw\DtW::getInstance()->user->id,
                ':response' => $response,
                ':weight' => $weight
            ));

            // Calculate if any action needs to be taken
            $responses = $this->getFlagVotes($flag);

            // Get post ID
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `post_id`, `user_id`
                FROM `forum_flags`
                WHERE `flag_id` = :flag
            '); 
            $stmt->execute(array(
                ':flag' => $flag
            ));
            $item = $stmt->fetch();

            $post = \dtw\DtW::getInstance()->discussions->getPost(null, $item->post_id);

            // Should it be deleted
            if (
                ($responses->delete >= 10 && $responses->dismiss < $responses->delete / 2) ||
                ($responses->delete >= 5 && $responses->dismiss <= 2)
            ) {
                // Delete post
                $post->delete(true);

                $this->resolveFlag($flag);

                // Notify author
                $notificationData = (object) array(
                    'flag' => $flag,
                    'thread' => $post->thread_id,
                    'post' => $post->id,
                    'status' => 'deleted'
                );
                \dtw\Notifications::send($post->author, 'discussion.moderation', $notificationData);

                // Notify flag author
                if ($item->user_id) {
                    $notificationData = (object) array(
                        'flag' => $flag,
                        'thread' => $post->thread_id,
                        'post' => $post->id,
                        'status' => 'resolved'
                    );
                    \dtw\Notifications::send($item->user_id, 'discussion.flag', $notificationData);
                }

            // Should the flag be closed
            } else if (
                ($responses->dismiss >= 10 && $responses->delete < $responses->dismiss / 2) ||
                ($responses->dismiss >= 5 && $responses->delete <= 2)
            ) {
                // Resolve flag
                $this->resolveFlag($flag, false);

                // Notify flag author
                if ($item->user_id) {
                    $notificationData = (object) array(
                        'flag' => $flag,
                        'thread' => $post->thread_id,
                        'post' => $post->id,
                        'status' => 'resolved'
                    );
                    \dtw\Notifications::send($item->user_id, 'discussion.flag', $notificationData);
                }

            // Should the flag be escalate
            } else if (
                $responses->dismiss >= 15 ||
                $responses->delete >= 15 ||
                $responses->escalate >= 10
            ) {
                $this->escalateFlag($flag);
            }
        }

        private function resolveFlag($flag, $accepted = true) {
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE `forum_flags` SET `resolved` = :resolved WHERE `flag_id` = :flag;
            ');
            $stmt->execute(array(
                ':resolved' => $accepted ? 1 : 2,
                ':flag' => $flag
            ));

            // Check user who created flag's medals
            $stmt = \dtw\DtW::$db->prepare("
                SELECT `user_id` FROM forum_flags WHERE `flag_id` = :flag
            ");
            $stmt->execute(array(
                ':flag' => $flag
            ));
            $userID = $stmt->fetchColumn();
            \dtw\Medals::checkUsersMedals('flags', $userID);
        }

        private function escalateFlag($flag) {
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE `forum_flags` SET `type` = \'other\' WHERE `flag_id` = :flag;
            ');
            $stmt->execute(array(
                ':flag' => $flag
            ));
        }

        public function getResolvedLogs() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT *
                FROM `forum_flags`
                INNER JOIN (SELECT flag_id, MAX(`time`) AS `time` FROM `forum_flags_moderation` GROUP BY flag_id) actions
                    ON `forum_flags`.`flag_id` = `actions`.`flag_id`
                WHERE `resolved` > 0
                ORDER BY `actions`.`time` DESC
                LIMIT 25;
            ');

            $stmt->execute();
            $flags = $stmt->fetchAll();

            foreach($flags AS $flag) {
                $flag->votes = $this->getFlagVotes($flag->flag_id);
            }


            return $flags;
        }

    }
