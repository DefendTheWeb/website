<?php
    namespace dtw\messages;

    class Message {

        public function __construct($ID = null) {
            $message = $this->message;
            $this->message = new \stdClass();
            $this->message->plain = $message;
            $this->message->safe = htmlspecialchars($message);
            $this->message->html = \dtw\utils\Markdown::parse($message, true);
        }

    }