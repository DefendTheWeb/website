<?php
    namespace dtw;

    class Statistics {
        /**
        * Get user list sorted by reputation
        *
        * @return array user IDs
        */
        public static function getLeaderboard($sort = 'reputation') {
            $key = 'stats:leaderboard:' . $sort;
            $leaderboard = \dtw\DtW::$redis->get($key);

            if ($leaderboard) {
                $leaderboard = json_decode($leaderboard);
            } else {
                $sort2 = $sort == 'points' ? 'reputation' : 'points';

                $stmt = \dtw\DtW::$db->prepare("
                    SELECT `users`.`user_id`
                    FROM `users`
                    LEFT OUTER JOIN `user_settings`
                    ON `user_settings`.`user_id` = `users`.`user_id` AND `user_settings`.`key` = 'privacy.leaderboard' AND `user_settings`.`value` = 1
                    WHERE `user_settings`.`user_id` IS NULL
                    ORDER BY `{$sort}` DESC, `{$sort2}` DESC, `username` ASC
                    LIMIT 25
                ");
                $stmt->execute();
                if ($stmt->rowCount()) {
                    $leaderboard = $stmt->fetchAll(\PDO::FETCH_COLUMN);

                    \dtw\DtW::$redis->set($key, json_encode($leaderboard));
                    \dtw\DtW::$redis->expire($key, 900);
                }
            }

            return $leaderboard;
        }

    }