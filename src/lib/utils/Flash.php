<?php
    namespace dtw\utils;

    class Flash {

        private static $flashes;

        public static function add($msg, $type = 'info', $group = 'generic') {
            if (!isset($_SESSION['flashes'])) {
                $_SESSION['flashes'] = array($group => array());
            } elseif (!isset($_SESSION['flashes'][$group])) {
                $_SESSION['flashes'][$group] = array();
            }

            $flash = new \stdClass();
            $flash->type = $type;
            $flash->msg = $msg;

            array_push($_SESSION['flashes'][$group], $flash);
        }

        public static function load() {
            // Store flashes for later requests
            if (isset($_SESSION['flashes'])) {
                self::$flashes = $_SESSION['flashes'];
                unset($_SESSION['flashes']);
            }
        }

        public static function get($group = 'generic') {
            if ($group === null) {
                $group = 'generic';
            }

            if (isset(self::$flashes) && isset(self::$flashes[$group])) {
                return self::$flashes[$group];
            }

            return array();
        }


    }