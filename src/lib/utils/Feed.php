<?php
    namespace dtw\utils;

    class Feed {
        public static function registerHooks() {
            \dtw\DtW::$api->registerHook('feed', 'history', function ($params) {
                return self::getFeed();
            });
        }

        public function getLatestDiscussions() {
            $DtW = \dtw\DtW::getInstance();
            $discussions = $DtW->load('Discussions');
            return $DtW->discussions->getList(1, 'all', 'latest');
        }

        public function getLatestExternalNews() {
            $cache = \dtw\DtW::$redis->get('feed:news.external');
            if ($cache) {
                return json_decode($cache);
            }
        }

        public function getLatestExploits() {
            $cache = \dtw\DtW::$redis->get('feed:news.exploits');
            if ($cache) {
                return json_decode($cache);
            }
        }

        public function updateNews() {
            $feed = array();

            // Get sources
            $sources = \Noodlehaus\Config::load(__DIR__ . '/../../../config/news.json');

            foreach ($sources AS $source => $url) {
                $xml = simplexml_load_file($url, "SimpleXMLElement", LIBXML_NOCDATA); //loading the document
                $items = $xml->channel->item; //gets the title of the document.

                if (!$items) {
                    echo $source;
                    continue;
                }

                $sourceLink = (string) $xml->channel->link;

                foreach ($items AS $item) {
                    $post = new \stdClass();
                    $post->type = "News";
                    $post->subtype = "External";
                    $post->title = (string) $item->title;
                    $post->link = (string) $item->link;
                    $post->date = (string) $item->pubDate;
                    $post->updated = $post->date;

                    $post->excerpt = (string) $item->description;

                    $post->source = new \stdClass();
                    $post->source->title = $source;
                    $post->source->link = $sourceLink;

                    $duplicate = false;
                    foreach ($feed as $temp) {
                        if ($post->link == $temp->link) {
                            $duplicate = true;
                            break;
                        }
                    }

                    if (!$duplicate) {
                        array_push($feed, $post);
                    }
                }
            }

            // Order news
            usort($feed, function($a, $b) {
                return strtotime($a->updated) < strtotime($b->updated);
            });

            // Just keep latest 24
            $feed = array_slice($feed, 0, 24);

            \dtw\DtW::$redis->set('feed:news.external', json_encode($feed));
        }

        public function updateExploits() {
            $feed = array();

            $xml = simplexml_load_file('https://www.exploit-db.com/rss.xml', "SimpleXMLElement", LIBXML_NOCDATA); //loading the document
            $items = $xml->channel->item; //gets the title of the document.

            foreach ($items AS $item) {
                $post = new \stdClass();
                $post->type = "News";
                $post->subtype = "Exploit";
                $post->title = (string) $item->title;
                $post->link = (string) $item->link;
                $post->date = (string) $item->pubDate;

                $post->excerpt = (string) $item->description;

                array_push($feed, $post);
            }

            // Just keep latest 5
            $feed = array_slice($feed, 0, 5);

            \dtw\DtW::$redis->set('feed:news.exploits', json_encode($feed));
        }

        public function getFeed() {
            $feed = \dtw\DtW::$redis->lrange('feed:global', 0, -1);

            return array_map(function($item) {
                return json_decode($item);
            }, $feed);
        }

        public static function addItemToFeed($data) {
            $data['timestamp'] = date(DATE_ISO8601);

            $DtW = \dtw\DtW::getInstance();

            if ($data['user']) {
                $DtW->load('Images');
                try {
                    $profile = new \dtw\user\Profile($data['user']);
                    $data['user'] = $profile->username;
                    $data['image'] = $DtW->images->get($profile->avatar, "navatar");
                } catch (\Exception $e) {
                    $data['image'] = $DtW->images->getDefault("navatar");
                }
            }

            \dtw\DtW::$redis->lpush('feed:global', json_encode($data));
            \dtw\DtW::$redis->ltrim('feed:global', 0, 10);
            \dtw\DtW::$redis->publish('feed', true);

            // Send
            if (!\dtw\DtW::$config->get('websocket.server')) return;

            $request = new \stdClass();
            $request->method = "publish";
            $request->params = new \stdClass();
            $request->params->channel = "feed";
            $request->params->data = $data;

            $request = json_encode($request);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://" . \dtw\DtW::$config->get('websocket.server') . '/api');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                array(
                    'Authorization:apikey ' . \dtw\DtW::$config->get('websocket.key'),
                    'Content-Type:application/json',
                    'Content-Length: ' . strlen($request)
                )
            );
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        }
    }