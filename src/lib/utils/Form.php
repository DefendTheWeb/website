<?php
    namespace dtw\utils;

    class Form {
        public function __construct($title = null, $submit = "Save", $extras = array()) {
            // Generate form ID for storing content
            $this->id = md5($_SERVER['REQUEST_URI'] . $title);

            $this->method = isset($extras['method']) ? $extras['method'] : "POST";
            $this->hideTitle = isset($extras['hideTitle']) ? $extras['hideTitle'] : false;
            $this->action = isset($extras['action']) ? $extras['action'] : null;
            $this->block = isset($extras['block']) ? $extras['block'] : true;
            $this->draft = isset($extras['draft']) ? $extras['draft'] : null;
            $this->description = isset($extras['description']) ? $extras['description'] : null;
            $this->footer = isset($extras['footer']) ? $extras['footer'] : null;

            $this->fields = array();
            $this->title = $title;
            $this->slug = \dtw\utils\Utils::slug($title);
            if ($title && !isset($extras['action'])) {
                $this->action = "?" . $this->slug;
            }
            $this->submit = $submit;

            $this->addField('token', 'hidden', array(
                'value' => \dtw\utils\CSRF::generate()
            ));

            // Add form ID for 
            $this->addField('formID', 'hidden', array(
                'value' => $this->id
            ));

            $key = '__form_' . $this->id;
            if (array_key_exists($key, $_SESSION)) {
                $storedResponse = $_SESSION[$key];
                if ($storedResponse) {
                    $this->storedResponse = unserialize($storedResponse);
                    $this->clearResponses();
                }
            }
        }

        public function addField($label, $type = "text", $extra = null) {
            $field = new \stdClass();
            $field->label = $label;
            $field->name = \dtw\utils\Utils::slug($field->label);
            $field->type = $type;

            $field = (object) array_merge((array) $field, (array) $extra);

            $storedResponse = $this->getResponse($field->name);
            if ($storedResponse) {
                $field->value = $storedResponse;
            }

            array_push($this->fields, $field);
        }

        public function getResponse($name) {
            if (in_array($name, array('token'))) {
                return;
            }

            if (isset($this->storedResponse)) {
                if (isset($this->storedResponse[$name])) {
                    return $this->storedResponse[$name];
                }
            }
        }

        public function clearResponses() {
            unset($_SESSION['__form_' . $this->id]);
        }

        public static function storeResponses() {
            if (isset($_POST['formid'])) {
                $formID = $_POST['formid'];
                $_SESSION['__form_' . $formID] = serialize($_POST);
            }
        }
    }
?>