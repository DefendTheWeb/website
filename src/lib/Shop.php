<?php
    namespace dtw;

    class Shop {

        public function __construct() {
            \Stripe\Stripe::setApiKey(\dtw\DtW::$config->get('stripe'));

            if (isset($_SESSION['shop'])) {
                $this->order = json_decode($_SESSION['shop']);
            } else {
                $this->order = new \stdClass();
            }

            $this->products = \dtw\DtW::$config->get('products');
        }


        public function getProducts() {
            try {
                return \Stripe\Product::all();
            } catch (\Exception $e) {
                throw new \Exception('Error loading product list');    
            }
        }


        public function getProduct($productID) {
            try {
                $product =  \Stripe\Product::retrieve([
                    'id' => $productID
                ]);
                $product->SKUs = $this->getSKUs($productID);

                return $product;
            } catch (\Exception $e) {
                throw new \Exception('Error loading product');    
            }
        }


        public function getSKUs($productID) {
            try {
                return \Stripe\SKU::all([
                    'product' => $productID,
                    'limit' => 100
                ]);
            } catch (\Exception $e) {
                throw new \Exception('Product not found');    
            }
        }

        public function getSKU($sku) {
            try {
                return \Stripe\SKU::retrieve([
                    'id' => $sku
                ]);
            } catch (\Exception $e) {
                throw new \Exception('Product not found');    
            }
        }

        public function buildProductForm($product) {
            // var_dump($product);

            $form = new \dtw\utils\Form($product->name, "Add to basket", array(
                'block' => false,
                'hideTitle' => true
            ));
            $form->addField('product', 'hidden', array('value' => $product->id));

            $extras = array(
                'hideLabel' => true,
                'options' => array()
            );
            foreach($product->SKUs AS $sku) {
                $extras['options'][$sku->id] = implode(', ', $sku->attributes->values());
            }
            $extras['options'] = array_reverse($extras['options']);
            $form->addField('sku', 'select', $extras);

            return $form;
        }



        public function addToOrder($product, $sku) {
            $items = array();
            if ($this->order->items) {
                $items = $this->order->items;
            }

            // Lookup item
            $item = $this->getProduct($product);
            if (!$item) {
                throw new \Exception('Error adding item to basket');
            }

            // Lookup SKU
            $itemSku = $this->getSKU($sku);
            if (!$itemSku) {
                throw new \Exception('Error adding item to basket');
            }

            $product = array(
                'productID' => $product,
                'sku' => $itemSku->id,
                'name' => $item->name,
                'details' => implode(', ', $itemSku->attributes->values()),
                'amount' => $item->metadata->price,
                'quantity' => 1
            );
            array_push($items, $product);

            $this->order->items = $items;
            $_SESSION['shop'] = json_encode($this->order);
        }

        public function donate($amount, $monthly = false) {
            preg_match('/(\d+)(\.(\d+))?/', $amount, $matches);

            $amount = ($matches[1]*100) + $matches[3];
            
            if ($amount < 1) {
                throw new \Exception('Invalid amount');
            }

            $items = array();

            $product = (object)array(
                'name' => 'Donation',
                'amount' => $amount,
                'quantity' => 1
            );
            array_push($items, $product);

            $this->order = new \stdClass();
            $this->order->items = $items;

            return $this->getStripeSessionID(true);
        }

        public function clearBasket() {
            unset($_SESSION['shop']);
        }

        public function getStripeSessionID($donation = false) {
            $items = array_map(function($item) {
                return array(
                    'name' => $item->name,
                    'amount' => $item->amount,
                    'currency' => 'gbp',
                    'quantity' => $item->quantity
                );
            }, $this->order->items);

            $args = [
                'customer_email' => \dtw\DtW::getInstance()->user->email,
                'payment_method_types' => ['card'],
                'line_items' => $items,
                'success_url' => \dtw\DtW::$config->get('site.domain') . '/shop/success',
                'cancel_url' => \dtw\DtW::$config->get('site.domain') . '/shop',
            ];

            if ($donation) {
                $args['billing_address_collection'] = 'auto';
                $this->order->type = 'donation';
                $args['submit_type'] = 'donate';
            } else {
                $args['billing_address_collection'] = 'required';
                $args['submit_type'] = 'pay';
                $this->order->type = 'sale';
            }

            try {
                $session = \Stripe\Checkout\Session::create($args);
            } catch (\Exception $e) {
                var_dump($args);
                var_dump($e);
                die();

                throw new \Expetion('Error creating order');    
            }

            if ($session->id) {
                $this->order->stripeSessionID = $session->id;

                $_SESSION['shop'] = json_encode($this->order);

                return $session->id;
            }
        }

        public function orderComplete() {
            if (!$this->order || !$this->order->stripeSessionID) {
                throw new \Exception('No order in progress');
            }

            // Check with stripe if session was complete
            try {
                $session = \Stripe\Checkout\Session::retrieve($this->order->stripeSessionID);
            } catch (\Exception $e) {
                throw new \Exception('Order not found');
            }

            if ($session->customer) {
                // Store in DB
                $stmt = \dtw\DtW::$db->prepare("
                    INSERT INTO `user_orders` (`user_id`, `stripe_id`, `stripe_customer`, `order`, `type`)
                    VALUES ( :user_id, :stripe_id, :stripe_customer, :order, :type );
                ");
                $stmt->execute(array(
                    ':user_id' => \dtw\DtW::getInstance()->user->id,
                    ':stripe_id' => $this->order->stripeSessionID,
                    ':stripe_customer' => $session->customer,
                    ':order' => json_encode($this->order->items),
                    ':type' => json_encode($this->order->type)
                ));
                $order_ = \dtw\DtW::$db->lastInsertId();

                $total = 0;
                foreach($this->order->items as $item) {
                    $total += $item->amount * $item->quantity;
                }

                unset($_SESSION['shop']);

                if ($this->order->type == 'donation') {
                    $this->saveDonation($order_, $total);

                    \dtw\Medals::checkUsersMedals('donate');

                    return 'donation';
                } else if ($this->order->type == 'sale') {
                    // // Complete order with scalablepress
                    // $scalablePressID = $this->processScalablePressOrder();

                    // $stmt = \dtw\DtW::$db->prepare("
                    //     UPDATE `user_orders` SET `scalablepress` = :scalablepress WHERE `order_id` = :order_id
                    // ");
                    // $stmt->execute(array(
                    //     ':order_id' => $order_id,
                    //     ':scalablepress' => $scalablePressID
                    // ));

                    return 'sale';
                }
            } else {
                throw new \Exception('Order not complete');
            }
        }




        // private function processScalablePressOrder($items) {
        //     $order = new \stdClass();

        //     $order->products = array();
        //     $order->products[0] = array();
        //     $order->products[0]['id'] = 'anvil-fashion-fit-t-shirt';
        //     $order->products[0]['color'] = 'navy';
        //     $order->products[0]['quantity'] = 1;
        //     $order->products[0]['size'] = 'lrg';

        //     $order->address = array();
        //     $order->address['name'] = '';
        //     $order->address['address1'] = '';
        //     $order->address['address2'] = '';
        //     $order->address['city'] = '';
        //     $order->address['state'] = '';
        //     $order->address['zip'] = '';
        //     $order->address['country'] = '';

        //     $quote = $this->getScalablePressQuote($order->products, $order->address);

        //     $order_id = $this->submitScalablePressOrder($quote);

        //     return $order_id;
        // }


        // public function getScalablePressQuote($products, $address) {
        //     $ch = curl_init();

        //     curl_setopt($ch, CURLOPT_URL,"https://api.scalablepress.com/v2/quote");
        //     curl_setopt($ch, CURLOPT_USERPWD, ":" . \dtw\DtW::$config->get('scalablePress'));
        //     curl_setopt($ch, CURLOPT_POST, 1);

        //     $data = array();
        //     $data['type'] = 'dtg';
        //     $data['designId'] = '58023f4c459f2d8241389bf0';
        //     $data['address'] = $address;
        //     $data['products'] = $products;
        //     curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //     $response = curl_exec($ch);

        //     curl_close($ch);

        //     $response = json_decode($response);

        //     if ($reponse->orderToken === null) {
        //         throw new \Exception('Order not created');
        //     }

        //     return $response->orderToken;
        // }

        // public function submitScalablePressOrder($orderToken) {
        //     $DtW = \dtw\DtW::getInstance();

        //     $ch = curl_init();

        //     curl_setopt($ch, CURLOPT_URL,"https://api.scalablepress.com/v2/order");
        //     curl_setopt($ch, CURLOPT_USERPWD, ":" . \dtw\DtW::$config->get('scalablePress'));
        //     curl_setopt($ch, CURLOPT_POST, 1);

        //     $data = array();
        //     $data['orderToken'] = $orderToken;
        //     curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //     $response = curl_exec($ch);

        //     $response = json_decode($response);

        //     if ($response->statusCode == '400') {
        //         throw new \Exception($response->message);
        //     }

        //     if ($response->status == 'order') {
        //         // Save users order
        //         $stmt = \dtw\DtW::$db->prepare("
        //             INSERT INTO `user_orders` (`user_id`, `token`)
        //             VALUES ( :user_id, :token);
        //         ");
        //         $stmt->execute(array(
        //             ':user_id' => $DtW->user->id,
        //             ':token' => $response->order_id
        //         ));
        //     }

        //     curl_close($ch);


        //     $response = json_decode($response);
        //     if ($reponse->orderId === null) {
        //         throw new \Exception('Order not submitted');
        //     }

        //     return $response->orderId;
        // }

        public function getUserOrders() {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth()) {
                return false;
            }

            $stmt = \dtw\DtW::$db->prepare('SELECT * FROM user_orders WHERE `user_id` = :userID ORDER BY `created` DESC'); 
            $stmt->execute(array(':userID' => $DtW->user->id)); 
            $orders = $stmt->fetchAll();

            foreach($orders AS &$order) {
                $order->items = json_decode($order->order);

                if ($order->scalablepress) {
                    $order->status = $this->getOrderStatus($order->scalablepress);
                }
            }

            return $orders;
        }

        private function getOrderStatus($token) {
            $DtW = \dtw\DtW::getInstance();

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"https://api.scalablepress.com/v2/order/" . $token);
            curl_setopt($ch, CURLOPT_USERPWD, ":" . \dtw\DtW::$config->get('scalablePress'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($response);

            if ($response->statusCode == '400') {
                throw new \Exception($response->message);
            }

            $status = new \stdClass();
            $status->message = 'Processing';
            foreach($response->events AS $event) {
                if ($event->name == "order") {
                    $status->message = 'Order placed';
                    $status->when = $event->createdAt;
                }
                if ($event->name == "printing") {
                    $status->message = 'Items being printed';
                    $status->when = $event->createdAt;
                }
                if ($event->name == "shipped") {
                    $status->message = 'Items dispatched';
                    $status->when = $event->createdAt;
                }
            }

            return $status;
        }

        private function saveDonation($order_id, $amount) {
            $stmt = \dtw\DtW::$db->prepare("
                INSERT INTO `user_donations` (`user_id`, `order_id`, `amount`)
                VALUES ( :user_id, :order_id, :amount );
            ");
            $stmt->execute(array(
                ':user_id' => \dtw\DtW::getInstance()->user->id,
                ':order_id' => $order_id,
                ':amount' => $amount / 100
            ));
        }

        public static function getDonations() {
            $stmt = \dtw\DtW::$db->prepare('SELECT * FROM user_donations ORDER BY `created` DESC');
            $stmt->execute(); 
            return $stmt->fetchAll();
        }

    }