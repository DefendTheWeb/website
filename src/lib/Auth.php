<?php
    namespace dtw;

    class Auth {
        public function __construct() {
            $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
            $this->currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
            $this->currentUri->setQuery('');
            $this->serviceFactory = new \OAuth\ServiceFactory();
            $this->storage = new \OAuth\Common\Storage\Session();

            $this->config = \dtw\DtW::$config->get('auth');
        }

        public function login($provider, $response = null) {
            switch ($provider) {
                case 'website': $this->loginWebsite(); break;
                case 'twitter': $this->loginTwitter($response); break;
                case 'google': $this->loginGoogle($response); break;
                case 'github': $this->loginGitHub($response); break;
                case 'facebook': $this->loginFacebook($response); break;
                default: return false;
            }

            return true;
        }

        public function register() {
            $user = new user\User();
            try {
                $user->create([
                    'username' => $_POST['username'],
                    'password' => $_POST['password'],
                    'email' => $_POST['email']
                ]);
            } catch (Exception $e) {
                throw $e;
            }
        }

        public static function forgot($request) {
            if (!$request || !strlen($request)) {
                throw new \Exception('Missing field');
            }

            // Lookup user
            try {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT user_id
                    FROM users
                    WHERE `username` = :request
                        OR `email` = :request
                '); 
                $stmt->execute(array(':request' => $request)); 
                if ($stmt->rowCount()) {
                    $userID = $stmt->fetchColumn();

                    $user = new user\Profile($userID);

                    $token = utils\Utils::generateToken();
                    \dtw\DtW::$redis->zAdd('auth:token:' . $user->id, time(), $token);

                    $info = new \stdClass();
                    $info->username = $user->name;
                    $info->link = \dtw\DtW::$config->get('site.domain') . '/auth/forgot?userID=' . $user->id . '&token=' . $token;

                    // Send reset email
                    \dtw\utils\Emailer::send($user->id, 'Password reset link', 'password-reset', $info);
                } else {
                    throw new \Exception('User not found');
                }
            } catch (Exception $e) {
                throw $e;
            }
        }

        public static function checkToken($userID, $token) {
            $key = 'auth:token:' . $userID;

            // Delete all members older than 10 minutes ago
            \dtw\DtW::$redis->zRemRangeByScore($key, 0, time() - 600);

            // Lookup token in pruned list
            $score = \dtw\DtW::$redis->zScore($key, $token);

            if ($score) {
                // Delete set
                \dtw\DtW::$redis->del($key);

                return true;
            } else {
                return false;
            }
        }

        private function loginWebsite() {
            $user = new user\User();
            $user->login($_POST['username'], $_POST['password'], isset($_POST['remember']));
        }

        private function loginTwitter($response) {
            $credentials = new \OAuth\Common\Consumer\Credentials(
                $this->config['twitter']['key'],
                $this->config['twitter']['secret'],
                $this->currentUri->getAbsoluteUri()
            );
            $twitterService = $this->serviceFactory->createService('twitter', $credentials, $this->storage);

            if (!empty($_GET['oauth_token'])) {
                $token = $this->storage->retrieveAccessToken('Twitter');
                $twitterService->requestAccessToken(
                    $_GET['oauth_token'],
                    $_GET['oauth_verifier'],
                    $token->getRequestTokenSecret()
                );
                $result = json_decode($twitterService->request('account/verify_credentials.json'));

                // Try and login
                if (\dtw\DtW::getInstance()->user->isAuth()) {
                    $user = \dtw\DtW::getInstance()->user;
                } else {
                    $user = new user\User();
                }
                if ($user->oauth('twitter', $result->id)) {
                    if (!empty($_SESSION['referer'])) {
                        $response->redirect($_SESSION['referer']);
                        unset($_SESSION['referer']);
                    } else {
                        $response->redirect('/dashboard');
                    }
                } else {
                    // Redirect user to complete profile
                    $user = array();
                    $user['token'] = $result->id;
                    $user['service'] = 'twitter';
                    $user['username'] = $result->screen_name;

                    $_SESSION['newUser'] = $user;

                    $response->redirect('/auth/setup');
                }
            } else {
                $token = $twitterService->requestRequestToken();
                $url = $twitterService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));
                header('Location: ' . $url);
            }
        }

        private function loginGoogle($response) {
            $credentials = new \OAuth\Common\Consumer\Credentials(
                $this->config['google']['ID'],
                $this->config['google']['secret'],
                $this->config['google']['callback']
            );

            $googleService = $this->serviceFactory->createService('google', $credentials, $this->storage, array('userinfo_email', 'userinfo_profile'));
            if (isset($_GET['state'])) {
                $state = isset($_GET['state']) ? $_GET['state'] : null;
                $googleService->requestAccessToken($_GET['code'], $state);
                $result = json_decode($googleService->request('userinfo'), true);

                // Try and login
                if (\dtw\DtW::getInstance()->user->isAuth()) {
                    $user = \dtw\DtW::getInstance()->user;
                } else {
                    $user = new user\User();
                }
                if ($user->oauth('google', $result['id'])) {
                    if (!empty($_SESSION['referer'])) {
                        $response->redirect($_SESSION['referer']);
                        unset($_SESSION['referer']);
                    } else {
                        $response->redirect('/dashboard');
                    }
                } else {
                    // Redirect user to complete profile
                    $user = array();
                    $user['token'] = $result['id'];
                    $user['service'] = 'google';
                    $user['email'] = $result['email'];

                    $_SESSION['newUser'] = $user;

                    $response->redirect('/auth/setup');
                }
            } else {
                $url = $googleService->getAuthorizationUri();
                header('Location: ' . $url);
            }
        }

        private function loginFacebook($response) {
            $credentials = new \OAuth\Common\Consumer\Credentials(
                $this->config['facebook']['ID'],
                $this->config['facebook']['secret'],
                $this->currentUri->getAbsoluteUri()
            );

            $facebookService = $this->serviceFactory->createService('facebook', $credentials, $this->storage, array('email'));
            if (!empty($_GET['code'])) {
                $state = isset($_GET['state']) ? $_GET['state'] : null;
                $token = $facebookService->requestAccessToken($_GET['code'], $state);
                $result = json_decode($facebookService->request('/me?fields=name,email'), true);

                // Try and login
                if (\dtw\DtW::getInstance()->user->isAuth()) {
                    $user = \dtw\DtW::getInstance()->user;
                } else {
                    $user = new user\User();
                }
                if ($user->oauth('facebook', $result['id'])) {
                    if (!empty($_SESSION['referer'])) {
                        $response->redirect($_SESSION['referer']);
                        unset($_SESSION['referer']);
                    } else {
                        $response->redirect('/dashboard');
                    }
                } else {
                    // Redirect user to complete profile
                    $user = array();
                    $user['token'] = $result['id'];
                    $user['service'] = 'facebook';
                    $user['email'] = $result['email'];

                    $_SESSION['newUser'] = $user;

                    $response->redirect('/auth/setup');
                }
            } else {
                $url = $facebookService->getAuthorizationUri();
                header('Location: ' . $url);
            }
        }

        private function loginGitHub($response) {
            $credentials = new \OAuth\Common\Consumer\Credentials(
                $this->config['github']['ID'],
                $this->config['github']['secret'],
                $this->currentUri->getAbsoluteUri()
            );

            $gitHub = $this->serviceFactory->createService('GitHub', $credentials, $this->storage, array('user'));
            if (!empty($_GET['code'])) {
                $gitHub->requestAccessToken($_GET['code']);
                $result = json_decode($gitHub->request('user'), true);

                // Try and login
                if (\dtw\DtW::getInstance()->user->isAuth()) {
                    $user = \dtw\DtW::getInstance()->user;
                } else {
                    $user = new user\User();
                }
                if ($user->oauth('github', $result['id'])) {
                    if (!empty($_SESSION['referer'])) {
                        $response->redirect($_SESSION['referer']);
                        unset($_SESSION['referer']);
                    } else {
                        $response->redirect('/dashboard');
                    }
                } else {
                    // Redirect user to complete profile
                    $user = array();
                    $user['token'] = $result['id'];
                    $user['service'] = 'github';
                    $user['username'] = $result['login'];
                    $user['email'] = $result['email'];
                    $user['avatar'] = $result['avatar_url'];

                    $_SESSION['newUser'] = $user;

                    $response->redirect('/auth/setup');
                }
            } else {
                $url = $gitHub->getAuthorizationUri();
                header('Location: ' . $url);
            }
        }
    }
?>