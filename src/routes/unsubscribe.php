<?php
    // Don't allow logged in users to see login page
    $this->respond('GET', '', function ($request, $response, $service, $app) {
        $args = array();

        // Check token
        $valid = \dtw\Auth::checkToken($_GET['id'], $_GET['token']);
        if (!$valid) {
            \dtw\utils\Flash::add('Invalid unsubscribe token', 'error');
            $response->redirect('/')->send();
            $this->skipRemaining();
        }

        $_SESSION['unsubscribe'] = $_GET['id'];

        $args['form'] = new \dtw\utils\Form("Unsubscribe", "unsubscribe", array(
            'block' => false,
            'hideTitle' => true
        ));

        return $app->DtW->tmpl->render('unsubscribe.twig', $args);
    });

    $this->respond('POST', '', function ($request, $response, $service, $app) {
        if (isset($_SESSION['unsubscribe'])) {
            $stmt = \dtw\DtW::$db->prepare('INSERT INTO `user_settings` (`user_id`, `key`, `value`) VALUES (:id, :key, :value) ON DUPLICATE KEY UPDATE `value` = :value'); 
            $stmt->execute(array(
                ':id' => $_SESSION['unsubscribe'],
                ':key' => 'emails.notifications',
                ':value' => false
            ));
            $stmt->execute(array(
                ':id' => $_SESSION['unsubscribe'],
                ':key' => 'emails.news',
                ':value' => false
            ));

            unset($_SESSION['unsubscribe']);

            \dtw\utils\Flash::add('You have been unsubscribed from all future emails', 'success');
        } else {
            \dtw\utils\Flash::add('There was an error unsubscribing you', 'error');
        }

        $response->redirect('/')->send();
        $this->skipRemaining();
    });