<?php
    $this->respond('GET', '', function($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        $args = array();
        $args['breadcrumb'] = array(
            'Chat' => '/chat'
        );

        $macaroon = $app->DtW->user->getMatrixToken('login');

        $args['homeserver'] = 'https://' . \dtw\DtW::$config->get('matrix.homeserver');
        $args['macaroon'] = $macaroon;

        return $app->DtW->tmpl->render('chat.twig', $args);
    });