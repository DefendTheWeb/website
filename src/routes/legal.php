<?php
    $this->respond('GET', '/privacy', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Legal' => '/legal',
            'Privacy Policy' => '/legal/privacy'
        );

        return $app->DtW->tmpl->render('legal/privacy.twig', array('breadcrumb' => $breadcrumb));
    });

    $this->respond('GET', '/terms', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Legal' => '/legal',
            'Terms of Use' => '/legal/terms'
        );

        return $app->DtW->tmpl->render('legal/terms.twig', array('breadcrumb' => $breadcrumb));
    });