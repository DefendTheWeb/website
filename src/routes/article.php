<?php
    $this->respond('GET', 's', function ($request, $response, $service, $app) {
        $args['breadcrumb'] = array(
            'Articles' => '/articles'
        );

        $args['topics'] = \dtw\Articles::getTopics();

        $args['articlePicks'] = \dtw\Articles::getPicks();

        if ($app->DtW->user->isAuth()) {
            $args['drafts'] = \dtw\Articles::getTopic('drafts');
        }

        return $app->DtW->tmpl->render('articles/index.twig', $args);
    });

    $this->respond('GET', 's/submit', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        $topics = \dtw\Articles::getTopics();


        $args = array();
  
        $args['breadcrumb'] = array(
            'Articles' => '/articles',
            'Submit article' => '/articles/submit'
        );

        $form = new \dtw\utils\Form('Submit article ', "Save", array('draft' => 'article:new'));
        $form->addField('Title', 'text');
        $form->addField('Content', "markdown");

        $extras = array();
        $extras['options'] = array();
        foreach($topics AS $topic) {
            $extras['options'][$topic->id] = $topic->title;
        }
        $form->addField('Topic', 'select', $extras);

        $args['form'] = $form;

        echo $app->DtW->tmpl->render('articles/article-editor.twig', $args);

        $response->send();
        $this->skipRemaining();
    });

    $this->respond('POST', 's/submit', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $article = \dtw\Articles::create($_POST);

            if ($article->slug) {
                \dtw\utils\Flash::add('Article created', 'success');
                // Redirect user to correct location
                $response->redirect('/article/' . $article->slug);
            } else {
                \dtw\utils\Flash::add('Article was not saved', 'error');
                $response->redirect($request->pathname());
            }
        } catch (Exception $e) {
            \dtw\utils\Form::storeResponses();
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $service->back();
        }

        $response->send();
        $this->skipRemaining();
    });

    $this->respond('GET', 's/[:topic]', function ($request, $response, $service, $app) {
        try {
            $topic = \dtw\Articles::getTopic($request->topic);
            $breadcrumb = array(
                'Articles' => '/articles',
                $topic->title => $topic->permalink
            );

            return $app->DtW->tmpl->render('articles/topics.twig', array('breadcrumb' => $breadcrumb, 'topic' => $topic));
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/articles');
        }
    });

    $this->respond('GET', '/[:slug]/[i:revision]?', function ($request, $response, $service, $app) {
        try {
            $article = \dtw\Articles::getArticle($request->slug);
            $topic = $article->getTopic();

            $args['canonical'] = \dtw\DtW::$config->get('site')['domain'] . $article->permalink;

            // Set opengraph data
            $args['meta'] = array(); 
            $args['meta']['og:type'] = 'article';
            $args['meta']['og:title'] = $article->title;
            $args['meta']['og:url'] = $args['canonical'];


            if ($article->image) {
                $app->DtW->load('Images');

                try {
                    $args['meta']['og:image'] = $app->DtW->images->get($article->image, 'medium');
                } catch (\Exception $e) {
                    //
                }
            }

            $args['article'] = $article;
            $args['breadcrumb'] = array(
                'Articles' => '/articles',
                $topic->title => $topic->permalink,
                $article->title => $article->permalink
            );

            if ($request->revision) {
                if ($article->isEditable()) {
                    $article->getRevision($request->revision);
                } else {
                    $response->redirect($article->permalink);
                }
            }

            return $app->DtW->tmpl->render('articles/article.twig', $args);
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $this->skipRemaining();
            return;
        }
    });

    $this->respond('POST', '/[:slug]/[i:revision]?', function ($request, $response, $service, $app) {
        try {
            $article = \dtw\Articles::getArticle($request->slug);

            if ($request->revision) {
                if ($article->isEditable()) {
                    $article->getRevision($request->revision);
                } else {
                    $response->redirect($article->permalink);
                }
            }

            if (isset($_POST['action'])) {
                try {
                    $article->handleModeration($_POST['action'], $_POST['message']);
                } catch (\Exception $e) {
                    \dtw\utils\Flash::add($e->getMessage(), 'error');
                }
                $service->back();
                return;
            }
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $service->back();
        }
    });


    // Show article edit page
    $this->respond('GET', '/[:slug]/edit', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $article = \dtw\Articles::getArticle($request->slug);

            if (!$article->isEditable()) {
                $response->redirect($article->permalink);
                $response->send();
                exit();
            }

            $topic = $article->getTopic();

            $args['breadcrumb'] = array(
                'Articles' => '/articles',
                $topic->title => $topic->permalink,
                $article->title => $article->permalink,
                'Edit' => $article->permalink . '/edit'
            );

            $args['article'] = $article;
            $args['editing'] = true;

            $topics = \dtw\Articles::getTopics();

            $form = new \dtw\utils\Form('Editing "' . $article->title . '"', "Save");
            $form->addField('Title', 'text', array(
                'value' => $article->title
            ));
            $form->addField('Content', "markdown", array(
                'value' => $article->content->plain
            ));

            $extras = array();
            $extras['value'] = $article->topic_id;
            $extras['options'] = array();
            foreach($topics AS $topic) {
                $extras['options'][$topic->id] = $topic->title;
            }
            $form->addField('Topic', 'select', $extras);

            $extras = array();
            if ($article->image) {
                $extras['src'] = $article->image;
                $extras['size'] = 'small';
            }
            $form->addField('Image', "upload", $extras);

            $args['form'] = $form;

            try {
                $args['revisions'] = $article->getRevisions();
            } catch (\Exception $e) {
                // No access to articles revisions
            }

            return $app->DtW->tmpl->render('articles/article-editor.twig', $args);
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/articles');
        }
    });


    // Save article edit
    $this->respond('POST', '/[:slug]/edit', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $article = \dtw\Articles::getArticle($request->slug);
            $update = $_POST;

            if (file_exists($_FILES['image']['tmp_name']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                $update['image'] = $_FILES['image'];
            }

            $slug = $article->update($update);

            \dtw\utils\Flash::add('Article updated', 'success');
            $response->redirect('/article/' . $slug . '/edit');
        } catch (Exception $e) {
            \dtw\utils\Form::storeResponses();
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $service->back();
        }
    });


    // Submit article for ewview
    $this->respond('GET', '/[:slug]/submit', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $article = \dtw\Articles::getArticle($request->slug);
            $article->setStatus('review');

            \dtw\utils\Flash::add('Article submitted for review', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect($article->permalink);
        $response->send();
    });