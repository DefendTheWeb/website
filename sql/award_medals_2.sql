INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `following`,
        case 
            when post_following LIKE 5 then 38
            when post_following LIKE 50 then 41
        end as `medal_id`, IFNULL(`followed`, NOW())
        FROM (
            SELECT
                `following`,
                `followed`,
                @post_following := IF(@post_follower = following, @post_following + 1, 1) AS post_following,
                @post_follower := following
            FROM user_following
            ORDER BY `following`, `followed` ASC
        ) following
    WHERE `post_following` IN (5, 50);

INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `author`,
        case 
            when post_rank LIKE 50 then 16
            when post_rank LIKE 250 then 17
            when post_rank LIKE 1000 then 18
        end as `medal_id`, `posted`
        FROM (
            SELECT
                `author`,
                `posted`,
                @post_rank := IF(@post_author = author, @post_rank + 1, 1) AS post_rank,
                @post_author := author
            FROM forum_thread_posts
            WHERE `deleted` = 0 AND `author` IS NOT NULL
            ORDER BY `author`, `posted` ASC
        ) ranked
    WHERE `post_rank` IN (50, 250, 1000);