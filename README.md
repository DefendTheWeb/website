Full source code for Defend the Web, an online security platform.

## Docker
There is a Docker image with instructions on how to get a test site setup quickly [here](docker).

## Manual setup
* Install nginx, php, mysql and setup
* Install php packages php-mbstring php-sqlite3 php-imagick
* Install redis server
```
unixsocket /tmp/redis.sock
unixsocketperm 755
```
* Clone repo
* Install node + npm + dependencies
```
cd DefendTheWeb
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
npm install
```
* Install composer and install dependencies
```
php composer.phar install --no-dev --optimize-autoloader
```
* Install discount markdown parser and gifsicle
* Import MySQL
```
mysql -u dtwstage -p dtwstage < sql/schema.sql
mysql -u dtwstage -p dtwstage < sql/data.sql
```
* Build project
```
./build.sh
```


## SQL data import
1. schema.sql
2. data.sql
3. import.sql
4. award_medals.sql
5. award_medals_2.sql manually
6. reputation.sql